const router = require('express').Router();
const db = require('../../database/index.js');

router.route('/doctors').get((req, res) => {
	db.query(
		'SELECT last_name, first_name FROM doctors',
		(error, results) => {
			if (error) {
				res.send({ error: true, data: results, message: error });
			}
			else {
				res.send(results);
			}

		}
	);
});

router.route('/doctors/appointments/:id').get((req, res) => {
	const query = `
		SELECT
			d.first_name,
			d.last_name,
			a.patient_last_name,
			a.patient_first_name,
			a.date_time,
			a.kind
			
			FROM 
				doctors d
				
			LEFT JOIN 
				appointments a
				ON d.id = a.doctor_id
				
			WHERE
				d.id = ${req.params.id}
		`

	db.query(
		query,
		(error, results) => {
			if (error) {
				res.send({ error: true, data: results, message: error });
			}
			else {
				res.send(results);
			}
		}
	);
});

router.route('/appointments/:id').delete((req, res) => {
	const query = `
		DELETE 
			a

		FROM
			appointments a
				
		WHERE
			id = ${req.params.id}
	`

	db.query(
		query,
		(error, results) => {
			if (error) {
				res.send({ error: true, data: results, message: error });
			}
			else {
				res.send({ error: false, data: results, message: 'Appointment has been Deleted successfully.' });
			}
		}
	);
});

router.route('/appointments').post((req, res) => {
	const dateTime = req.body.dateTime.toString();
	const minutes = dateTime.slice(14, 16);
	const kind = req.body.kind;
	const patientFirstName = req.body.patientFirstName;
	const patientLastName = req.body.patientLastName;
	const doctorId = req.body.doctorId;
	const confirmedTime = confirmTime(minutes);

	if (!confirmedTime) {
		res.send({ error: true, data: '', message: 'Appointment must be scheduled at a 15 minute interval. Example: 12:00, 12:15, 12:30, or 12:45' });
		return;
	}

	const countQuery = `
		SELECT
			COUNT(a.date_time) count

		FROM
			appointments a
		
		LEFT JOIN
			doctors d
			ON d.id = a.doctor_id
		
		WHERE
			a.date_time = CAST('${dateTime}' AS DateTime)
			AND d.id = '${doctorId}'
	`

	const postQuery = `
		INSERT INTO appointments (patient_first_name, patient_last_name, date_time, kind, doctor_id)
		VALUES (
			'${patientFirstName}', 
			'${patientLastName}',
			'${dateTime}',
			'${kind}',
			'${doctorId}'
		);	
	`

	db.query(
		countQuery,
		(error, results) => {
			if (error) {
				res.send({ error: true, data: results, message: error });
			}
			else {
				console.log('results: ', results);
				if (results[0].count > 3) {
					res.send({ error: true, data: results, message: 'There are already 3 appointments scheduled for that date/time. Please select a different date/time.' });
				}
				else if (results[0].count < 3) {
					db.query(
						postQuery,
						(error, results) => {
							if (error) {
								res.send({ error: true, data: results, message: error });
							}
							else {
								res.send({ error: false, data: results, message: 'Appointment has been Created successfully.' });
							}
						}
					);
				}
			}
		}
	);
});

const confirmTime = (minutes) => {
	if (minutes !== '00'
	&& minutes !== '15'
	&& minutes !== '30'
	&& minutes !== '45') {
		return false
	}

	return true;
};

module.exports = router;